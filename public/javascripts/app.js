/**
 * Created by dineshr on 1/11/2016.
 */
var app = angular.module("testApp",['uiGmapgoogle-maps']).config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyAus6Kc9QM2_boI_TFMZsdgY6PTRfP-ITw',
        v: '3.20', //defaults to latest 3.X anyhow
        libraries: 'weather,geometry,visualization'
    });
});

app.controller("testController",function($scope,uiGmapGoogleMapApi){
    $scope.test = {} ;
    $scope.test.titel = "Angular ";
    $scope.lat = '';
    $scope.lng = '';

    $scope.tests = [
        {t:1,
            t3:[{r:1},{r:2},{r:3}]
        },


    ];
    $scope.models = [];
    var infoWindow = new google.maps.InfoWindow();
    $scope.showPosition = function (position) {

        $scope.accuracy = position.coords.accuracy;
        $scope.$apply();

        //var latlng = new google.maps.LatLng($scope.lat, $scope.lng);
        //$scope.model.myMap.setCenter(latlng);
        //$scope.myMarkers.push(new google.maps.Marker({ map: $scope.model.myMap, position: latlng }));
    };
    $scope.showError = function (error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                $scope.error = "User denied the request for Geolocation."
                break;
            case error.POSITION_UNAVAILABLE:
                $scope.error = "Location information is unavailable."
                break;
            case error.TIMEOUT:
                $scope.error = "The request to get user location timed out."
                break;
            case error.UNKNOWN_ERROR:
                $scope.error = "An unknown error occurred."
                break;
        }
        $scope.$apply();
    }

    $scope.autoResult = '';
    $scope.autoOptions = null;
    //$scope.autoDetails = '';?\

    $scope.drawPin = function () {
     console.log($scope.autoDetails.geometry.location.lat());
        $scope.lat =  $scope.autoDetails.geometry.location.lat();
        $scope.lng =  $scope.autoDetails.geometry.location.lng();
        var changeMarker = {
            id: Date.now(),
            label: "test",
            options : {
                'draggable':true
            },
            coords: {
                latitude: $scope.autoDetails.geometry.location.lat(),
                longitude: $scope.autoDetails.geometry.location.lng()
            },
            events: {
                dragend: function (marker, eventName, args) {
                    $scope.lat = marker.getPosition().lat();
                    $scope.lng = marker.getPosition().lng();
                },
                click : function(marker, eventName, args){

                }
            }
        };
        $scope.map.markers.push(changeMarker);
        $scope.map.center.latitude =  $scope.lat;
        $scope.map.center.longitude =  $scope.lng;
        refresh(changeMarker);
        //var geocoder = new google.maps.Geocoder();
        //geocoder.geocode({'address' :$scope.address}, function (results, status) {
        //    if (status === google.maps.GeocoderStatus.OK) {
        //        var firstAddress = results[0];
        //        var latitude = firstAddress.geometry.location.lat();
        //        var longitude = firstAddress.geometry.location.lng();
        //        var changeMarker = {
        //            id: Date.now(),
        //            label: "test",
        //            options : {
        //                'draggable':true
        //            },
        //            coords: {
        //                latitude: latitude,
        //                longitude: longitude
        //            },
        //            events: {
        //                dragend: function (marker, eventName, args) {
        //                    $scope.lat = marker.getPosition().lat();
        //                    $scope.lng = marker.getPosition().lng();
        //                }
        //            }
        //        };
        //        $scope.map.markers.push(changeMarker);
        //        refresh(changeMarker);
        //    } else {
        //        alert("Unknown address: " + address);
        //    }
        //});

    };
    if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(function(position){
            $scope.lat = position.coords.latitude;
            $scope.lng = position.coords.longitude;
            uiGmapGoogleMapApi.then(function(maps) {
                var intialMarker = {
                    id: Date.now(),
                    label: "test",
                    options : {
                        'draggable':true
                    },
                    coords: {
                        latitude: $scope.lat,
                        longitude: $scope.lng
                    },
                    events: {
                        dragend: function (marker, eventName, args) {
                            $scope.lat = marker.getPosition().lat();
                            $scope.lng = marker.getPosition().lng();
                        }
                    }
                };


                $scope.map = {
                    center: {
                        latitude:  $scope.lat,
                        longitude:$scope.lng
                    },
                    zoom: 12,
                    control: {},
                    markers: [],
                    events: {
                        click: function (mapModel, eventName, originalEventArgs) {
                            $scope.lat = originalEventArgs[0].latLng.lat();
                            $scope.lng = originalEventArgs[0].latLng.lng()

                            var marker = {
                                id: Date.now(),
                                options : {
                                    'draggable':true
                                },
                                coords: {
                                    latitude: $scope.lat,
                                    longitude: $scope.lng
                                },
                                events: {
                                    dragend: function (marker, eventName, args) {
                                        $scope.lat = marker.getPosition().lat();
                                        $scope.lng = marker.getPosition().lng();
                                    }
                                }
                            };
                            $scope.map.markers.push(marker);
                            $scope.$apply();
                        }
                    },
                    marker: {
                        options: { draggable: true },
                        events: {
                            dragend: function (marker, eventName, args) {
                                $scope.lat = marker.getPosition().lat();
                                $scope.lng = marker.getPosition().lng();
                            }
                        }
                    }
                };
                $scope.map.markers.push(intialMarker);
            });
        });
    }else{
        console.log('Geo location is not supported');
    }
    function refresh(marker) {
        $scope.map.control.refresh({latitude: marker.latitude,
            longitude: marker.longitude});
    }
});

app.directive('ngAutocomplete', function($parse) {
    return {

        scope: {
            details: '=',
            ngAutocomplete: '=',
            options: '='
        },

        link: function(scope, element, attrs, model) {

            //options for autocomplete
            var opts

            //convert options provided to opts
            var initOpts = function() {
                opts = {}
                if (scope.options) {
                    if (scope.options.types) {
                        opts.types = []
                        opts.types.push(scope.options.types)
                    }
                    if (scope.options.bounds) {
                        opts.bounds = scope.options.bounds
                    }
                    if (scope.options.country) {
                        opts.componentRestrictions = {
                            country: scope.options.country
                        }
                    }
                }
            }
            initOpts()

            //create new autocomplete
            //reinitializes on every change of the options provided
            var newAutocomplete = function() {
                scope.gPlace = new google.maps.places.Autocomplete(element[0], opts);
                google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                    scope.$apply(function() {
//              if (scope.details) {
                        scope.details = scope.gPlace.getPlace();
//              }
                        scope.ngAutocomplete = element.val();
                    });
                })
            }
            newAutocomplete()

            //watch options provided to directive
            scope.watchOptions = function () {
                return scope.options
            };
            scope.$watch(scope.watchOptions, function () {
                initOpts()
                newAutocomplete()
                element[0].value = '';
                scope.ngAutocomplete = element.val();
            }, true);
        }
    };
});